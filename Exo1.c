/* Pour les constantes EXIT_SUCCESS et EXIT_FAILURE */
#include <stdlib.h>

/* Pour fprintf() */
#include <stdio.h>

/* Pour fork() */
#include <unistd.h>

/* Pour perror() et errno */
#include <errno.h>

/* Pour le type pid_t */
#include <sys/types.h>

/* Pour wait() */
#include <sys/wait.h>

/* La fonction creer_processus duplique le processus appelant et retourne le PID du processus fils ainsi créé */
pid_t creer_processus()	
{
	pid_t PID; // Pour récupérer l'identifiant du processus qu'on va creer

	// On fork (creer un processus) tant que l'erreur est EAGAIN
	do
	{
		PID = fork();
	}	while ( (PID == -1) && (errno == EAGAIN) ) ;

	return PID;
}


int main(int argc, char *argv[])
{
	char *commande1[] = { "who",  NULL };
	char *commande3[] = { "ls", "-l",  NULL };
	
	pid_t PID = creer_processus() ;
	switch (PID)
	{
		case -1:	// Erreur, on exit -1
			perror("fork");
			
			exit (-1);

		case 0:		// On est dans le processus fils qui vient d'être creé
			// Execution de la 1ere commande
			if (execvp("who", commande1) == -1)
			{
				perror("execvp");

				exit(-1);
			}
			// Après avoir exécuté la commande, on retourne dans le processus père

		default: 	// On est dans le processe père
			wait(NULL);

			chdir(getenv("HOME")); // Pour cd on utilise la fonction chdir au lieu de la fonction exec
			
			PID = creer_processus() ;	// On cree un nouveau processus pour executer la commande 3
			switch (PID)
			{
				case -1: 
					perror("fork");

					exit(-1);

				case 0:		// On est dans le nouveau processus fils (celui de la commande 3)
					if (execvp("ls", commande3) == -1)
					{
						perror("execvp");

						exit (-1);
					}
					// Après avoir exécuté la commande, on retourne dans le processus père

				default: // Processus père
					wait(NULL);

					break;

			break;



			}


	}

	return 0;
} 