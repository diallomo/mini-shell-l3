#include <stdio.h>
#include <stdlib.h>

#include <unistd.h> // pour utiliser fork

#include <sys/types.h>

#include <sys/wait.h>

int main(int argc, char *argv[]){
	int p[2];
	int p2[2];
	pipe(p);
	pipe(p2);
	
	// pour la commande ps ax
	char *commande1[] = {"ps", "ax", (char *)0};
	// pour la commande grep bash 
	char *commande2[] = {"grep", "bash", (char *)0};
	
	// pour la commande wc -l 
	char *commande3[] = {"wc", "-l", (char *)0};
	
	////////////////////////////////////////
	switch(fork()){
		case -1 : 
			fprintf(stderr, "Erreur de fork(niveau du ps ax)\n");
			break;
			
		case 0 : 
			printf("Fils 1");
			close(1);
			close(p[0]);
			
			dup(p[1]);
			close(p[1]);
			if (execvp(commande1[0], commande1) == -1) {
				perror("Il y'a une erreur avec le ps ax\n");
				exit(-1);
			}
			break;
		default : 
			switch(fork()){
				case -1 : 
					fprintf(stderr, "Erreur de fork(niveau du grep bash)\n");
					break;
				
				case 0 : 
					printf("Fils 2");
					// en gros ici je fait p et p2 puisque je suis au milieu 
					close(0);
					close(p[1]);
			
					dup(p[0]);
					close(p[0]);
					
					close(1);
					close(p2[0]);
			
					dup(p2[1]);
					close(p2[1]);
					
					if (execvp(commande2[0], commande2) == -1) {
						perror("Il y'a une erreur avec le grep bash\n");
						exit(-1);
					}
					break;
				default : 
					// ici je ferme le premier pipe 
					close(p[1]);
					close(p[0]);
					
					switch(fork()){
						case -1 : 
							fprintf(stderr, "Erreur de fork(niveau du grep bash)\n");
							break;
						case 0 : 
							printf("Fils 3");
							close(0);
							close(p2[1]);
			
							dup(p2[0]);
							close(p2[0]);
			
							if (execvp(commande3[0], commande3) == -1) {
							perror("Il y'a une erreur avec le wc -l\n");
							exit(-1);
							}
							break;
						default :
							// ici je ferme le deuxième pipe 
							close(p2[1]);
							close(p2[0]);
							break;
					
					}
					//while (wait(NULL)!=-1);
					break;
				
			}	
			//while (wait(NULL)!=-1);
			break;
			
	}
	//while (wait(NULL)!=-1);
	return 0;

}







/*if (pipe(p)) == -1){
			perror("Problème avec pipe (1)");
		}*/
		
