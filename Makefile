CC=clang
EXEC=mini-shell mini-shell_2.0

all: $(EXEC)

mini-shell: mini-shell.o
	$(CC) -o mini-shell mini-shell.o

mini-shell_2.0: mini-shell_2.0.o
	$(CC) -o mini-shell_2.0 mini-shell_2.0.o


mini-shell.o: mini-shell.c
	$(CC) -o mini-shell.o -c mini-shell.c

mini-shell_2.0.o: mini-shell_2.0.c
	$(CC) -o mini-shell_2.0.o -c mini-shell_2.0.c


clean:
	rm -rf $(EXEC) *.o
