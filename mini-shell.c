/* Pour les constantes EXIT_SUCCESS et EXIT_FAILURE */
#include <stdlib.h>

/* Pour fprintf() */
#include <stdio.h>

/* Pour fork() */
#include <unistd.h>

/* Pour perror() et errno */
#include <errno.h>

/* Pour le type pid_t */
#include <sys/types.h>

/* Pour wait() */
#include <sys/wait.h>

/* Pour les fonctions string */
#include <string.h>

/* Pour les fonctions liés au signal */
#include <signal.h>

/* Tableu de chaine de caractères contenant les variables d'environnement */
extern char **environ;




/*

Binôme : Meziane Farid et Diallo Mohamed Saliou

Lecture d'une commande;
Tant que la commande de sortie n'est pas tapée
	Analyse de la commande;
	Si la commande correspond à une fonction interne
	alors application de cette fonction aux arguments de la commande;
	sinon création d'un processus fils qui exécutera la commande;
		  et le processus père attend la mort du processus fils;
	Finsi
	Lecture d'une commande;
Fin tantque

*/

// -----------------------------------------------------------------------------

/* La fonction creer_processus duplique le processus appelant et retourne le PID du processus fils ainsi créé */
pid_t creer_processus()
{
	pid_t PID; // Pour récupérer l'identifiant du processus qu'on va creer

	// On fork (creer un processus) tant que l'erreur est EAGAIN
	do
	{
		PID = fork();
	}	while ( (PID == -1) && (errno == EAGAIN) ) ;

	return PID;
}


// -----------------------------------------------------------------------------

void Lire_commande(char *commande, char **tab_commande)
{
	int i = 0;
	char *sep = " \n";

	tab_commande[i] = strtok(commande, sep);

	do
	{
		i++;
		tab_commande[i] = strtok(NULL, sep);
	} while (tab_commande[i] != NULL);

	int j = 0;

	printf("\nCommande lu : \n");
	for(j = 0; j < i; j++)
	{
		printf("%s, ", tab_commande[j]);
	}
	printf("\n");

}

// -----------------------------------------------------------------------------

void traite_status(int a, int nb)
{

}


void mon_shell(char *arge[])
{
	int no, status;
	char cmd[100];			// commande lu dans le terminal
	char *com[10];			// tableau contenant les commandes lu dans le terminal
	char *variable_env;		// variable d'environnement du getenv
	int i_env; 			// compteur pour la boucle d'affichage des variables d'environnement
	pid_t PID;			// pour les processus qu'on va creer

	// Pour rendre le shell intouchable
	
	signal(SIGINT, SIG_IGN);	// signal permet d'intercepter un signal (ici SIGINT : provoquer l'interruption du processus)
					// et de definir un comportement (ici SIG_IGN : ignorer le signal) pour ce signal

	printf("Voici mon shell, taper Q pour sortir\n");
	printf("> ");
	fgets(cmd, 100, stdin);

	while (strcmp(cmd,"Q\n") != 0)
	{
		Lire_commande(cmd, com);

		/* Pour les commandes internes : */

		// cd
		if (strcmp(com[0],"cd") == 0)
		{
			printf("\nSTART cd\n\n");

			if (com[1] == NULL)
			{
				chdir(getenv("HOME")); // cd ~
			}
			else

			{
				if (chdir(com[1]) == -1) // cd le_chemin_dans_com[1]
				{
					perror(com[1]);
					// exit(-1);
				}
			}

		}

		// setenv
		else if (strcmp(com[0],"setenv") == 0)
		{
			printf("\nSTART setenv\n\n");

			if (com[1] == NULL)
			{
				// Nouveau commentaire

				// Affichage des variables d'environnement
				for (i_env = 0; environ[i_env] != NULL; i_env++)
				{
					printf("%s\n", environ[i_env]);
				}

			}
			else
			{
				// setenv -> com0	variable -> com1	valeur 	-> com2
				if(setenv(com[1], com[2], 1) == -1)	
				{
					errno = -1;
					perror("setenv");
					// exit(-1);
				}
               
			}
		}

		// getenv
		else if (strcmp(com[0],"getenv") == 0)
		{
			printf("\nSTART getenv\n\n");
			
			if(com[1] == NULL)
			{
				errno = 1;
				perror("getenv");
				// exit(-1);
			}

			variable_env = getenv(com[1]);
			
			if(variable_env == NULL)
			{
				errno = 1;
				perror("getenv");
				// exit(-1);
			}
			else
			{
				printf("%s=%s\n", com[1], variable_env);
			}


		}

		/* Pour les autres types de commandes : */
		else
		{	PID = creer_processus();
			
			switch (PID)
			{
				case -1 :	
					perror("fork");
					// exit(-1);

				case 0 : // On est dans le processus fils
					if (execvp(com[0], com) == -1)
					{
						perror("execvp");

						//exit (-1);
					}
					// Après avoir exécuté la commande, on retourne dans le processus père
					

				default :
					no = wait(&status);
					printf("Etat de retour du processus %d : %d\n", no, status);
					traite_status(no,status);
			}
		}

		printf("> ");
		fgets(cmd, 100, stdin);

	}
	
	printf("\nSortie\n");
}

int main(int argc,char *argv[],char *arge[])
{
	mon_shell(arge);
	return 0;
}
