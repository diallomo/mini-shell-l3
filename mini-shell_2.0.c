 /* Pour les constantes EXIT_SUCCESS et EXIT_FAILURE */
#include <stdlib.h>

/* Pour fprintf() */
#include <stdio.h>

/* Pour fork() */
#include <unistd.h>

/* Pour perror() et errno */
#include <errno.h>

/* Pour le type pid_t */
#include <sys/types.h>

/* Pour wait() */
#include <sys/wait.h>

/* Pour les fonctions string */
#include <string.h>

/* Pour les fonctions liés au signal */
#include <signal.h>

// Pour utiliser open et ses IDENTIFIER
#include <sys/stat.h>
#include <fcntl.h>

/* Tableu de chaine de caractères contenant les variables d'environnement */
extern char **environ;

// Redirection d'entrée (0 si redirection, -1 sinon)
int in;

// Redirection de sortie (0 si redirection, -1 sinon)
int out;

// Pipe
int p;

// Commande 1
char com1[10];

// Commande 2
char *com2;

// Nom du fichier d'entréé (redirection)
char *input;

// Nom du fichier de sortie (redirection)
char *output;



/*
		Auteurs : DIALLO 	Mohamed Saliou
				  MEZIANE 	Farid
*/

// -----------------------------------------------------------------------------

/* La fonction creer_processus duplique le processus appelant et retourne le PID du processus fils ainsi créé */
pid_t creer_processus()
{
	pid_t PID; // Pour récupérer l'identifiant du processus qu'on va creer

	// On fork (creer un processus) tant que l'erreur est EAGAIN
	do
	{
		PID = fork();
	}	while ( (PID == -1) && (errno == EAGAIN) ) ;

	return PID;
}


// -----------------------------------------------------------------------------

void Lire_commande(char *commande, char **tab_commande)
{
	int i = 0, i_recherche;
	int trouve = -1;
	int trouve_pipe = -1;
	char *sep = " \n";
	in = -1;
	out = -1;
	p = -1;


	int l = strlen(commande);

	// On recherche le caractère pour le pipe
	i_recherche = 0;
	while ((i_recherche < l) && (commande[i_recherche] != '|'))
	{
		i_recherche++;
	}

	if(i_recherche < l)
	{
		trouve_pipe = i_recherche;
		p = 0;
	}



	if(trouve_pipe != -1)
	{
		// On recupère la première commande dans com1
		strncpy(com1, commande, trouve_pipe);


		// On recupère la deuxième commande dans com2
		com2 = strchr(commande, '|');
		com2 = strchr(com2, com2[2]);
		
		
	}


	// On recherche le caractère de redirection "<" entrée
	i_recherche = 0;
	while ((i_recherche < l) && (commande[i_recherche] != '<'))
	{
		i_recherche++;
	}

	if(i_recherche < l)
	{
		trouve = i_recherche;
		in = 0;
	}

	// On recherche le caractère de redirection ">" sortie
	i_recherche = 0;
	while ((i_recherche < l) && (commande[i_recherche] != '>'))
	{
		i_recherche++;
	}

	if(i_recherche < l)
	{
		trouve = i_recherche;
		out = 0;
	}

	
	if(trouve != -1) // si on a trouvé des redirections
	{
		// On va copier la première commande à executer dans com
		char com[trouve];


		strncpy(com, commande, trouve);

		// On va copier la redirection dans red
		char *red;


		if (in == 0)
		{
			red = strchr(commande, '<');

			// On recupère le nom du fichier d'entrée
			strtok(red, " ");
			input = strtok(NULL, sep);
			printf("input : %s\n", input);


		}

		if (out == 0)
		{
			red = strchr(commande, '>');

			// On recupère le nom du fichier de sortie
			strtok(red, " ");
			output = strtok(NULL, sep);
			printf("output : %s\n", output);
		}
		

		// On met com dans le tableau de commandes
		tab_commande[i] = strtok(com, sep);
		do
		{
			i++;
			tab_commande[i] = strtok(NULL, sep);

		} while ((tab_commande[i] != NULL));
	}
	else // sinon
	{
		// On met commandes dans le tableau de commandes
		tab_commande[i] = strtok(commande, sep);
		do
		{
			i++;
			tab_commande[i] = strtok(NULL, sep);

		} while ((tab_commande[i] != NULL));

	}

	int j = 0;

	printf("Commande lu : ");

	for(j = 0; j < i; j++)
	{
		printf("%s, ", tab_commande[j]);
	}
	printf("\n");
	printf("\n");





}

// -----------------------------------------------------------------------------

void traite_status(int a, int nb)
{

}


void mon_shell(char *arge[])
{
	int no, status;
	char cmd[100];				// commande lu dans le terminal
	char *com[10];				// tableau contenant les commandes lu dans le terminal
	char *variable_env;			// variable d'environnement du getenv
	int i_env; 					// compteur pour la boucle d'affichage des variables d'environnement
	pid_t PID;					// pour les processus qu'on va creer
	pid_t PID2;					// pour le pid du 2 eme processus du pipe
	int fd_in;					// descripteur de fichier pour in
	int fd_out;					// descripteur de fichier pour out

	int p_tab1[2];				// tableau de descripteur 1 pour pipe	
	int p_tab2[2];				// tableau de descripteur 2 pour pipe	




	// Pour rendre le shell intouchable
	signal(SIGINT, SIG_IGN);	// signal permet d'intercepter un signal (ici SIGINT : provoquer l'interruption du processus)
														// et de definir un comportement (ici SIG_IGN : ignorer le signal) pour ce signal

	printf("Voici mon shell, taper Q pour sortir\n");
	printf("> ");
	fgets(cmd, 100, stdin);

	while (strcmp(cmd,"Q\n") != 0)
	{
		Lire_commande(cmd, com);

		/* Pour les commandes internes : */

		// cd
		if (strcmp(com[0],"cd") == 0)
		{
			printf("\nSTART cd\n\n");

			if (com[1] == NULL)
			{
				chdir(getenv("HOME")); // cd ~
			}
			else

			{
				if (chdir(com[1]) == -1) // cd le_chemin_dans_com[1]
				{
					perror(com[1]);
					// exit(-1);
				}
			}

		}

		// setenv
		else if (strcmp(com[0],"setenv") == 0)
		{
			printf("\nSTART setenv\n\n");

			if (com[1] == NULL)
			{
				// Nouveau commentaire

				// Affichage des variables d'environnement
				for (i_env = 0; environ[i_env] != NULL; i_env++)
				{
					printf("%s\n", environ[i_env]);
				}

			}
			else
			{
				// setenv -> com0	variable -> com1	valeur 	-> com2
				if(setenv(com[1], com[2], 1) == -1)
				{
					errno = -1;
					perror("setenv");
					// exit(-1);
				}

			}
		}

		// getenv
		else if (strcmp(com[0],"getenv") == 0)
		{
			printf("\nSTART getenv\n\n");

			if(com[1] == NULL)
			{
				errno = 1;
				perror("getenv");
				// exit(-1);
			}

			variable_env = getenv(com[1]);

			if(variable_env == NULL)
			{
				errno = 1;
				perror("getenv");
				// exit(-1);
			}
			else
			{
				printf("%s=%s\n", com[1], variable_env);
			}
		}
		/* Pour les autres types de commandes : */
		else
		{
			PID = creer_processus() ;	// On cree un nouveau processus pour executer la commande
			switch (PID)
			{
				case -1 :
					perror("fork");
					// exit(-1);

				case 0 : // On est dans le processus fils
					

					if (p == 0) // Si c'est une commande avec pipe
					{
						/*
						pipe(p_tab1);
						pipe(p_tab2)
						*/

						Lire_commande(com1, com);

						printf("---%s---\n", com1);

						printf("Fils 1\n");
						
						/*

						close(1);
						close(p_tab1[0]);

						dup(p_tab1[1]);
						close(p_tab1[1]);

						*/


						printf("Start %s\n", com[0]);
						if (execvp(com[0], com) == -1)
						{

							perror("execvp");
							//exit (-1);
						}
						// Après avoir exécuté la commande, on retourne dans le processus père


						
					}
					else
					{

						printf("Start %s\n", com[0]);

						
						// Redirections
						if (in == 0) // Redirection de l'entrée <
						{

								fd_in = open(input, O_RDONLY); // On crée un descripteur pour le fichier entrée

								if(fd_in < 0)	// si open n'a pas réussi a créer le descripteur
								{
									perror("open");
									// exit(-1);
								}



								// On utilise dup2 pour copier le contenu de fd_in en entrée
								dup2(fd_in, STDIN_FILENO);
								close(fd_in); // On ferme le descripteur
						}

						if (out == 0) // Redirection de la sortie >
						{
								fd_out = open(output, O_WRONLY); // On crée un descripteur de fichier sortie

								if(fd_in < 0)	// si open n'a pas réussi a créer le descripteur
								{
									perror("open");
									// exit(-1);
								}

								// On utilise dup2 pour copier le sortie sur fd_out
								dup2(fd_out, STDOUT_FILENO);
								close(fd_out); // On ferme le descripteur
						}
				
						
						if (execvp(com[0], com) == -1)
						{

							perror("execvp");
							//exit (-1);
						}
						// Après avoir exécuté la commande, on retourne dans le processus père
					}

					


				default :
					if(p == 0)
					{
						// On crée un processus pour la prochaine instruction
						PID2 = creer_processus();
						switch(PID2)
						{
							case -1 :
								perror("fork");
								// exit(-1);

							case 0 : // On est dans le nouveau processus fils

								/*
								close(0);
								close(p_tab1[1]);
						
								dup(p_tab1[0]);
								close(p_tab1[0]);
								
								close(1);
								close(p_tab2[0]);
						
								dup(p_tab2[1]);
								close(p_tab2[1]);
								*/


								Lire_commande(com2, com);

								printf("Fils 2\n");

								printf("Start %s\n", com[0]);



								if (execvp(com[0], com) == -1)
								{

									perror("execvp");
									//exit (-1);
								}
								// Après avoir exécuté la commande, on retourne dans le processus père

							default: // père
								no = wait(&status);
								printf("Etat de retour du processus %d : %d\n", no, status);
								traite_status(no,status);
						}	
					}

					else
					{
						no = wait(&status);
						printf("Etat de retour du processus %d : %d\n", no, status);
						traite_status(no,status);
					}
			}
		}

		printf("> ");
		fgets(cmd, 100, stdin);

	}

	printf("\nSortie\n");
}

int main(int argc,char *argv[],char *arge[])
{
	mon_shell(arge);
	return 0;
}

// Bang